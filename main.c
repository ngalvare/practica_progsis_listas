#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>

typedef struct nodo {
	short mes;
	short dia;
	short hora;
	float temperatura;
	struct nodo* siguiente;
} nodo_t;

nodo_t *cabecera = NULL;
nodo_t *actual = NULL;

//Variable globale que determina si la opción -c está siendo usada
bool cflag = false; //Opción -c, ignora la línea de cabecera

void recorrer_lista();

int main (int argc, char *argv[])
{
	int opt;
	char *nombre_archivo;
	FILE *fp;
	nodo_t *enlace, *nodo_anterior;

	while ((opt = getopt (argc, argv, "ch")) != -1){
		switch(opt)
		{
			case 'c':
				cflag = true;
				break;
			case 'h':
				printf("Lee un archivo tabulado con datos de temperatura.\n");
				printf("uso: %s [-c] [-h] archivo\n", argv[0]);
				return 0;
			case '?':
			default:
				fprintf(stderr, "uso: %s [-c] [-h] archivo\n", argv[0]);
				return -1;
		}
	}

	if(cflag && argc==3)
		nombre_archivo = argv[2];
	else if(!cflag && argc==2)
		nombre_archivo = argv[1];
	else{
		fprintf(stderr, "Número incorrecto de argumentos.\n");
		fprintf(stderr, "uso: %s [-c] [-h] archivo\n", argv[0]);
		return -1;
	}

	printf("Abriendo el archivo %s",nombre_archivo);
	if(cflag)
		printf(" y asumiendo que tiene cabecera.\n");
	else
		printf(".\n");

	fp = fopen(nombre_archivo,"r");

	if(cflag)
		//Ignora la primera línea...
		fscanf(fp, "%*[^\n]\n");

	int n;
	short mes,dia,hora;
	float temperatura;
	do{
		n = fscanf(fp,"%hd\t%hd\t%hd\t%f",&mes,&dia,&hora,&temperatura);
		if(n > 3){
			//printf("%hd\t%hd\t%hd\t%.2f\n",mes,dia,hora,temperatura);
			enlace = (nodo_t *) malloc(sizeof(nodo_t));
			enlace->mes = mes;
			enlace->dia = dia;
			enlace->hora = hora;
			enlace->temperatura = temperatura;
			enlace->siguiente = nodo_anterior;
			cabecera = enlace;
		}

	}while(n > 0);

	recorrer_lista();
}

void recorrer_lista()
{
	nodo_t *ptr;
	ptr = cabecera;

	while(ptr != NULL) {
		printf("%hd\t%hd\t%hd\t%.2f\n",ptr->mes,ptr->dia,ptr->hora,ptr->temperatura);
		ptr = ptr->siguiente;
	}
}